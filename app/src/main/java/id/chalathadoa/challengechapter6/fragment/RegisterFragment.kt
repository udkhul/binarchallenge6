package id.chalathadoa.challengechapter6.fragment

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import id.chalathadoa.challengechapter6.Communicator
import id.chalathadoa.challengechapter6.R
import id.chalathadoa.challengechapter6.data.UserManager
import id.chalathadoa.challengechapter6.databinding.FragmentRegisterBinding
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class RegisterFragment : Fragment() {

    private var _binding: FragmentRegisterBinding? = null
    private val binding get() = _binding!!

    //create global variabel to manage usermanager
    private lateinit var userManager: UserManager
    private lateinit var comm: Communicator
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        _binding = FragmentRegisterBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onDestroy() {
        super.onDestroy()
        _binding = null
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        //getting references to userPreferences
        userManager = UserManager(requireContext())
        onRegisClicked()
        toFragmentLoginClicked()
    }

    private fun onRegisClicked(){
        binding.btnSubmitLogin.setOnClickListener {
            storeRegis()
        }
    }

    //save data regis from user
    private fun storeRegis() {
        val username = binding.etUsername.text.toString().trim()
        val email = binding.etEmail.text.toString().trim()
        val password = binding.etPassword.text.toString().trim()

        /**
         * because the class is suspend function, so it's run only inside coroutine scope
         */

        if (username.isNotEmpty() && email.isNotEmpty() && password.isNotEmpty()){
            lifecycleScope.launch(Dispatchers.Main) {
                userManager.saveLoginInfo(email, password)
                //getUsernameValue()
                findNavController().navigate(R.id.action_registerFragment_to_homeFragment)
                Toast.makeText(requireContext(), "Register Succesfull!", Toast.LENGTH_SHORT).show()
            }
        } else {
            Toast.makeText(requireContext(), "Please fill all the field", Toast.LENGTH_SHORT).show()
        }
    }

    private fun toFragmentLoginClicked() {
        binding.tvSudahPunyaAkun.setOnClickListener {
            findNavController().navigate(R.id.action_registerFragment_to_loginFragment)
        }
    }

    private fun getUsernameValue(){
        comm = requireContext() as Communicator
        comm.passUsername(binding.etUsername.text.toString())
    }
}