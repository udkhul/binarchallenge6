package id.chalathadoa.challengechapter6.fragment

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import id.chalathadoa.challengechapter6.R
import id.chalathadoa.challengechapter6.data.UserManager
import id.chalathadoa.challengechapter6.databinding.FragmentHomeBinding
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class HomeFragment : Fragment() {
    private  var _binding : FragmentHomeBinding? = null
    private val binding get() = _binding!!

    private lateinit var userManager: UserManager
    private var username: String? = ""

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        _binding = FragmentHomeBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onDestroy() {
        super.onDestroy()
        _binding = null
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        userManager = UserManager(requireContext())
        setUsername()
        logout()
    }

    private fun setUsername() {
        username = arguments?.getString("USERNAME")
        binding.tvWelcomeHome.setText(username)

    }

//    private fun welcomeUsername() {
//        lifecycle.coroutineScope.launchWhenCreated {
//            userManager.getUserName().collect {
//                binding.tvWelcomeHome.text= it
//            }
//        }
//    }

    private fun logout(){
        binding.btnLogout.setOnClickListener {
            lifecycleScope.launch(Dispatchers.Main){
                userManager.deleteLogin()
                findNavController().navigate(R.id.action_homeFragment_to_loginFragment)
            }
            //viewModel.clearLogin()
        }
    }
}