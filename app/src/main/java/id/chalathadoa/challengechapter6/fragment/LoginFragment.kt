package id.chalathadoa.challengechapter6.fragment

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import id.chalathadoa.challengechapter6.R
import id.chalathadoa.challengechapter6.data.UserManager
import id.chalathadoa.challengechapter6.databinding.FragmentLoginBinding
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class LoginFragment : Fragment() {

    private var _binding: FragmentLoginBinding? = null
    private val binding get() = _binding!!

    private lateinit var userManager:UserManager
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        _binding = FragmentLoginBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onDestroy() {
        super.onDestroy()
        _binding = null
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        userManager = UserManager(requireContext())
        onLoginClicked()
        toFragmentSignUpClicked()
    }

    private fun onLoginClicked() {
        binding.btnSubmitLogin.setOnClickListener {
            getDataLogin()
        }
    }

    private fun toFragmentSignUpClicked(){
        binding.tvBelumPunyaAkun.setOnClickListener {
            findNavController().navigate(R.id.action_loginFragment_to_registerFragment)
        }
    }

    private fun getDataLogin(){
        val email = binding.etEmail.text.toString().trim()
        val password = binding.etPassword.text.toString().trim()

        if (email.isNotEmpty() && password.isNotEmpty()){
            lifecycleScope.launch(Dispatchers.Main){
                val pass = userManager.readLoginInfo(email)
                if (pass.equals(password)){
                    findNavController().navigate(R.id.action_loginFragment_to_homeFragment)
                    Toast.makeText(requireContext(), "Login succesfull!", Toast.LENGTH_SHORT).show()
                } else {
                    Toast.makeText(requireContext(), "Email or Password is wrong!", Toast.LENGTH_SHORT).show()
                }
            }
        } else {
            Toast.makeText(requireContext(), "All field must filled!", Toast.LENGTH_SHORT).show()
        }
    }
}