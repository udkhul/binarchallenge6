package id.chalathadoa.challengechapter6.network

import okhttp3.ResponseBody

/**
 * Class untuk menghandle API responses. success and failure
 */
sealed class Resource<out T> {

    //class to wrap success response
    data class Success<out T>(val value: T): Resource<T>()

    //class to wrap error response -- won't return T cause it has failure situation.
    //but return values that required to handle failure
    data class Failure(
        val isNetworkError: Boolean,
        val errorCode: Int?,
        val errorBody: ResponseBody?
    )
}