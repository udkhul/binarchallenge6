package id.chalathadoa.challengechapter6.data

import android.content.Context
import android.os.Bundle
import androidx.datastore.core.DataStore
import androidx.datastore.dataStore
import androidx.datastore.preferences.core.Preferences
import androidx.datastore.preferences.core.edit
import androidx.datastore.preferences.core.stringPreferencesKey
import androidx.datastore.preferences.preferencesDataStore
import id.chalathadoa.challengechapter6.Communicator
import id.chalathadoa.challengechapter6.fragment.HomeFragment
import kotlinx.coroutines.flow.map
//import this instead concurrent
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.flow.first

class UserManager (context: Context): Communicator {
    //create data store
    private val Context.datastore: DataStore<Preferences> by preferencesDataStore("login")
    private val mDataStore: DataStore<Preferences> = context.datastore

    //to get username value

    //create keys value to store and retrieve data

    //create name, email, and password flow to retrieve name from preferences
//    fun getUserName() = mDataStore.data.map {
//        it[USER_NAME_KEY] ?: ""
//    }


    //new try
    suspend fun saveLoginInfo(key:String, value:String){
        val loginkey = stringPreferencesKey(key)
        mDataStore.edit { login ->
            login[loginkey] = value
        }
    }

    suspend fun readLoginInfo(key: String):String{
        val loginkey = stringPreferencesKey(key)
        val preferences = mDataStore.data.first()
        return preferences[loginkey].toString()
    }

    suspend fun deleteLogin(){
        mDataStore.edit { preferences ->
            preferences.clear()
        }
    }

    override fun passUsername(editTextInput: String) {
        val bundle = Bundle()
        bundle.putString("USERNAME", editTextInput)

        val fragHome = HomeFragment()
        fragHome.arguments = bundle
    }
}