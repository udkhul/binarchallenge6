package id.chalathadoa.challengechapter6

interface Communicator {
    fun passUsername(editTextInput: String)
}